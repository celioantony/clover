package br.cantony.clover.interfaces;

import br.cantony.clover.model.entity.MyGames;

/**
 * Created by celio on 25/03/18.
 */

public interface OnItemClickListenerMyGames {
    void onItemShortClick(MyGames itemMyMegaSena);
    void onItemLongClick(MyGames itemMyMegaSena, int position);
}
