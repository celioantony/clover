package br.cantony.clover.interfaces;

/**
 * Created by celio on 29/03/18.
 */

public interface DialogFinish {
    void finish(Boolean result);
}
