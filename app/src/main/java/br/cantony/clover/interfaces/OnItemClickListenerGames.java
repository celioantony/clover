package br.cantony.clover.interfaces;

import br.cantony.clover.model.entity.Games;

/**
 * Created by celio on 21/03/18.
 */

public interface OnItemClickListenerGames {
    void onItemClick(Games itemGame);
}
