package br.cantony.clover.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.cantony.clover.R;
import br.cantony.clover.adapter.NumbersGamesAdapter;
import br.cantony.clover.helper.GamesHelper;
import br.cantony.clover.helper.MyGamesHelper;
import br.cantony.clover.model.dao.MyGamesDAO;
import br.cantony.clover.model.entity.MyGames;

/**
 * Created by celio on 25/03/18.
 */

public class GamesFragment1 extends android.support.v4.app.Fragment {

    private static final String TAG = "GamesFragment1";

    //    helper
    private MyGamesHelper myGamesHelper;
    private GamesHelper gamesHelper;
//    recyclerview
    private NumbersGamesAdapter adapterNumbers;
    private ArrayList<Integer> numbersDataSet;

//  View
    private RecyclerView rvNumbersGenerated;
    private ImageView btnGenerateNumbers;
    private ImageView btnSaveNumbers;

    private String type;

//    private int[] numbers = {1,2,3,4,5,6};
//    private TextView tvNumbersGenerated;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.games_fragment1, container, false);
        myGamesHelper = new MyGamesHelper(view);
        setElements(view);
        setGridNumbers();

//        get type game
        type = getArguments().getString("type");

//        set up click in components
        btnGenerateNumbers.setOnClickListener(new GenerateNumbers());
        btnSaveNumbers.setOnClickListener(new SaveNumbers());

        return view;
    }


    private void setElements(View view) {
//        tvNumbersGenerated = (TextView) view.findViewById(R.id.ms_numbers_generated);
        rvNumbersGenerated = (RecyclerView) view.findViewById(R.id.rv_numbers_generated);
        btnGenerateNumbers = (ImageView) view.findViewById(R.id.ms_btn_generate_numbers);
        btnSaveNumbers = (ImageView) view.findViewById(R.id.ms_btn_save_numbers);
    }

    class GenerateNumbers implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            numbersDataSet = myGamesHelper.generateNumbers(type);
            adapterNumbers.update(numbersDataSet);

        }
    }

    class SaveNumbers implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            gamesHelper = new GamesHelper();

            if(numbersDataSet.size() < 5 ) {
                Toast.makeText(getContext(),
                        "Números invalidos! ¯\\_(ツ)_/¯",
                        Toast.LENGTH_SHORT).show();
            } else {
                MyGames myGames = myGamesHelper.getMyGames(numbersDataSet, type);

                MyGamesDAO myMegaSenaDAO = new MyGamesDAO(getContext());
                myMegaSenaDAO.add(myGames);

                Toast.makeText(getContext(),
                        "Jogo da "+gamesHelper.getDescriptionGame(type)+" salvo!",
                        Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void setGridNumbers() {
        int numberOfColumns = 6;
        numbersDataSet = new ArrayList<>();
//        for(int i=0;i<numberOfColumns;i++){
//            numbersDataSet.add(0);
//        }

        rvNumbersGenerated.setLayoutManager(
                new GridLayoutManager(getContext(), numberOfColumns));
        adapterNumbers = new NumbersGamesAdapter(numbersDataSet);
        rvNumbersGenerated.setAdapter(adapterNumbers);
    }
}
