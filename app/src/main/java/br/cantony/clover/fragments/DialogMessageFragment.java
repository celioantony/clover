package br.cantony.clover.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import br.cantony.clover.R;
import br.cantony.clover.interfaces.DialogFinish;
import br.cantony.clover.model.dao.MyGamesDAO;
import br.cantony.clover.model.entity.MyGames;

/**
 * Created by celio on 29/03/18.
 */

@SuppressLint("ValidFragment")
public class DialogMessageFragment extends DialogFragment {

    private DialogFinish dialogFinish;
    private MyGames myGame;

    @SuppressLint("ValidFragment")
    public DialogMessageFragment(DialogFinish dialogFinish) {
        this.dialogFinish = dialogFinish;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
//        get args
        myGame = (MyGames) getArguments().getSerializable("myGame");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.msg_confirm)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

//                        delete instance
                        MyGamesDAO myGamesDAO = new MyGamesDAO(getContext());
                        myGamesDAO.delete(myGame);
                        myGamesDAO.close();

                        Toast.makeText(getContext(), R.string.yes_confirm, Toast.LENGTH_LONG)
                                .show();

                        dialogFinish.finish(true);

                    }
                })
                .setNegativeButton(R.string.no_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(getContext(),
                                R.string.msg_no_dialog, Toast.LENGTH_LONG)
                                .show();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
