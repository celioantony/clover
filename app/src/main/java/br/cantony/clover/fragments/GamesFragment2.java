package br.cantony.clover.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.cantony.clover.R;
import br.cantony.clover.model.entity.MyGames;

/**
 * Created by celio on 25/03/18.
 */

public class GamesFragment2 extends android.support.v4.app.Fragment {

    private static final String TAG = "GamesFragment2";

     private ArrayList<MyGames> myMegaSenasDataSet;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.games_fragment2, container, false);

        return view;
    }



    @Override
    public void onStop() {
        super.onStop();
    }
}
