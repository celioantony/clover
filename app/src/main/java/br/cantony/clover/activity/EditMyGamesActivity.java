package br.cantony.clover.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.cantony.clover.R;
import br.cantony.clover.adapter.NumbersGamesAdapter;
import br.cantony.clover.model.dao.MyGamesDAO;
import br.cantony.clover.model.entity.MyGames;

public class EditMyGamesActivity extends AppCompatActivity {

    private static final String[] options = {"Sim", "Não"};

    //    componentes
    private Spinner spCompetition;
//    private TextView tvNumbersGenerated;
    private TextView tvDateEmit;
    private EditText etCompetitionNumbers;
    private Spinner spWin;
    private EditText etComment;
    private Button btnSave;

//    recyclerview
    private RecyclerView rvNumbersGenerated;
    private NumbersGamesAdapter adapterNumbers;
    private ArrayList<Integer> numbersDataSet;

//  Entity
    private MyGames myMegaSena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_games);
//        get objects
        myMegaSena = (MyGames) getIntent().getSerializableExtra("myGame");
//        get components
        getComponents();
//        settings components
        settingsComponentes();
//        set values in components
        setValuesInComponents(myMegaSena);

//        save values
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValuesInComponents();
            }
        });
        setGridNumbers();
    }

    private void settingsComponentes() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, options);
//        ArrayAdapter<String> adapterWin = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_dropdown_item, options);

        spCompetition.setAdapter(adapter);
        spWin.setAdapter(adapter);
    }

    private void getComponents() {
//        tvNumbersGenerated = (TextView) findViewById(R.id.tv_label_numbers_generated);
        tvDateEmit = (TextView) findViewById(R.id.tv_label_date_emit);
        etCompetitionNumbers = (EditText) findViewById(R.id.et_competition_number);
        spCompetition = (Spinner) findViewById(R.id.sp_competition);
        spWin = (Spinner) findViewById(R.id.sp_win);
        etComment = (EditText) findViewById(R.id.et_comment_mygame);
        btnSave = (Button) findViewById(R.id.btn_save_mygame);
        rvNumbersGenerated = (RecyclerView) findViewById(R.id.rv_edit_game);
    }

    private void setValuesInComponents(MyGames myMegaSena) {
//        tvNumbersGenerated.setText(myMegaSena.getDezenasText());
        tvDateEmit.setText(myMegaSena.getDataGerada());

        if(myMegaSena.getConcursoApostado() != 0){
            etCompetitionNumbers.setText(String.valueOf(myMegaSena.getConcursoApostado()));
        }

        Boolean competition = myMegaSena.getApostado();
        Boolean win = myMegaSena.getGanhou();
        if(competition){
            spCompetition.setSelection(0);
        } else {
            spCompetition.setSelection(1);
        }
        if(win){
            spWin.setSelection(0);
        } else {
            spWin.setSelection(1);
        }
        etComment.setText(myMegaSena.getComentario());
    }

    private void getValuesInComponents() {

        String competitionStr = etCompetitionNumbers.getText().toString();
        if ((competitionStr != null) && !competitionStr.isEmpty()) {
            int value = Integer.parseInt(competitionStr);
            myMegaSena.setConcursoApostado(value);
        }
        Boolean isCompetition = spCompetition.getSelectedItemPosition() == 0;
        myMegaSena.setApostado(isCompetition);
        Boolean isWin = spWin.getSelectedItemPosition() == 0;
        myMegaSena.setGanhou(isWin);
        myMegaSena.setComentario(etComment.getText().toString());

        MyGamesDAO myMegaSenaDAO = new MyGamesDAO(this);
        myMegaSenaDAO.update(myMegaSena);
        myMegaSenaDAO.close();

        Toast.makeText(this,
                "Os dados do jogo foram atualizados com sucesso.",
                Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(EditMyGamesActivity.this, GamesActivity.class);
        intent.putExtra("type", myMegaSena.getTipo());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(EditMyGamesActivity.this, GamesActivity.class);
        intent.putExtra("type", myMegaSena.getTipo());
        startActivity(intent);
    }

    private void setGridNumbers() {
        int numberOfColumns = 6;
        numbersDataSet = new ArrayList<>();
        int[] dezenas = myMegaSena.getDezenas();

        for(int i = 0; i < dezenas.length; i++) {
            numbersDataSet.add(dezenas[i]);
        }

        rvNumbersGenerated.setLayoutManager(
                new GridLayoutManager(this, numberOfColumns));
        adapterNumbers = new NumbersGamesAdapter(numbersDataSet);
        rvNumbersGenerated.setAdapter(adapterNumbers);
    }
}
