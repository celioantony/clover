package br.cantony.clover.activity;


import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import br.cantony.clover.R;
import br.cantony.clover.adapter.MyGamesAdapter;
import br.cantony.clover.adapter.SectionsPageAdapter;
import br.cantony.clover.fragments.DialogMessageFragment;
import br.cantony.clover.fragments.GamesFragment1;
import br.cantony.clover.fragments.GamesFragment2;
import br.cantony.clover.helper.GamesHelper;
import br.cantony.clover.interfaces.DialogFinish;
import br.cantony.clover.interfaces.OnItemClickListenerMyGames;
import br.cantony.clover.model.dao.MyGamesDAO;
import br.cantony.clover.model.entity.MyGames;

public class GamesActivity extends AppCompatActivity {

    private static final String TAG = "GamesActivity";
    private Toolbar toolbar;

    private String type;
//    Tabs
    private SectionsPageAdapter msSectionsPageAdapter;
    private ViewPager msViewPager;
    String initialTab;

//    recycerView
    private ArrayList<MyGames> myDataSet;
    private RecyclerView rvMyGames;
    private RecyclerView.Adapter myGamesAdapter;
    private RecyclerView.LayoutManager myGamesLayoutManager;

    private MyGames myGame;
    private int position;
    private GamesHelper gamesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_games);
        gamesHelper = new GamesHelper();
//        type game
        type = getIntent().getStringExtra("type");

//        get tab initial
        initialTab = getIntent().getStringExtra("tab");

        msSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

//        set up the ViewPager
        msViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(msViewPager);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(msViewPager);

        tabLayout.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener(){

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        switch (tab.getPosition()) {
                            case 1:
                                rvMyGames = (RecyclerView) findViewById(R.id.rv_my_games);
                                loadList();
                                registerForContextMenu(rvMyGames);
                                break;
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        Log.d(TAG, "onTabUnselected: "+tab.getPosition());
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        Log.d(TAG, "onTabReselected: "+tab.getPosition());
                    }
                });

//        settings action bar
        setupToolBar();
    }

    public void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        GamesFragment1 fragment1 = new GamesFragment1();
        GamesFragment2 fragment2 = new GamesFragment2();

        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        fragment1.setArguments(bundle);
        fragment2.setArguments(bundle);

        adapter.addFragment(fragment1, "Gerar Jogo");
        adapter.addFragment(fragment2, "Seus Jogos");
        viewPager.setAdapter(adapter);


//        if(initialTab != null){
//            msViewPager.setCurrentItem(Integer.parseInt(initialTab));
//        }
    }

    private void loadList() {
//        load DataSet
        MyGamesDAO myGamesDAO = new MyGamesDAO(this);
        myDataSet = myGamesDAO.list(type);
        int regSize = myGamesDAO.count();
        myGamesDAO.close();
//        set RecyclerView
        rvMyGames.setHasFixedSize(true);
//        set LayoutManager
        myGamesLayoutManager = new LinearLayoutManager(this);
        rvMyGames.setLayoutManager(myGamesLayoutManager);

//        set up click
        myGamesAdapter = new MyGamesAdapter(myDataSet,
                new OnItemClickListenerMyGames() {

            @Override
            public void onItemShortClick(MyGames itemMyMegaSena) {
                Toast.makeText(getApplicationContext(),
                        "Short Click.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemLongClick(MyGames itemMyGame, int pos) {
                myGame = itemMyGame;
                position = pos;
            }

        });
        rvMyGames.setAdapter(myGamesAdapter);

        Log.d(TAG, "Reg size: "+regSize+" myDataSet size: "+myDataSet.size());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_mygames, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.menu_editar:
                intent = new Intent(GamesActivity.this, EditMyGamesActivity.class);
                intent.putExtra("myGame", myGame);
                startActivity(intent);
                break;

            case R.id.menu_excluir:
                DialogMessageFragment dialogMessage =
                        new DialogMessageFragment(new DialogFinish() {

                    @Override
                    public void finish(Boolean result) {
                        Log.d(TAG, "result: "+result);
                        myDataSet.remove(myGame);
                        myGamesAdapter.notifyItemRemoved(position);
                        myGamesAdapter.notifyItemRangeChanged(position, myDataSet.size());
                    }
                });

                Bundle args = new Bundle();
                args.putSerializable ("myGame", myGame);
                dialogMessage.setArguments(args);
                dialogMessage.show(getSupportFragmentManager(), TAG);

        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(GamesActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void setupToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(gamesHelper.getDescriptionGame(type));
    }
}
