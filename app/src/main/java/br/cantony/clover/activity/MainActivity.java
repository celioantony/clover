package br.cantony.clover.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

import br.cantony.clover.R;
import br.cantony.clover.adapter.GamesAdapter;
import br.cantony.clover.helper.GamesHelper;
import br.cantony.clover.interfaces.OnItemClickListenerGames;
import br.cantony.clover.model.entity.Games;

public class MainActivity extends AppCompatActivity {

//    Button btnMegaSena;
    private RecyclerView rvGames;
    private RecyclerView.Adapter gamesAdapter;
    private RecyclerView.LayoutManager gamesLayoutManager;
    private ArrayList<Games> gamesDataSet;

//    helpers
    private GamesHelper gamesHelper = new GamesHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gamesDataSet = gamesHelper.getListGames();

        rvGames = (RecyclerView) findViewById(R.id.rv_games);
        rvGames.setHasFixedSize(true);
        gamesLayoutManager = new LinearLayoutManager(this);
        rvGames.setLayoutManager(gamesLayoutManager);


        gamesAdapter = new GamesAdapter(gamesDataSet, new OnItemClickListenerGames() {
            @Override
            public void onItemClick(Games itemGame) {
                manageActivities(itemGame);
            }
        });
        rvGames.setAdapter(gamesAdapter);

    }

    public void manageActivities(Games itemGame) {
        Intent intent = new Intent(MainActivity.this, GamesActivity.class);
        intent.putExtra("type", itemGame.getInitials());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.about:
                Intent intent = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
