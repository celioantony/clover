package br.cantony.clover.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.cantony.clover.R;

import java.util.ArrayList;

import br.cantony.clover.interfaces.OnItemClickListenerGames;
import br.cantony.clover.model.entity.Games;

/**
 * Created by celio on 21/03/18.
 */

public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.ViewHolder> {

    private final OnItemClickListenerGames listener;
    private ArrayList<Games> gamesList;

    public GamesAdapter(ArrayList<Games> gamesList, OnItemClickListenerGames listener) {
        this.gamesList = gamesList;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public CardView cvItemsGames;
        public TextView tvItemsGames;

        public ViewHolder(View itemView) {
            super(itemView);

            tvItemsGames = (TextView) itemView.findViewById(R.id.tv_items_games);
            cvItemsGames = (CardView) itemView.findViewById(R.id.cv_items_games);
        }

        public void bind(final Games item, final OnItemClickListenerGames listener) {
            cvItemsGames.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    @Override
    public GamesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_layout_games, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GamesAdapter.ViewHolder holder, int position) {
        holder.tvItemsGames.setText(gamesList.get(position).getName());
        holder.bind(gamesList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return gamesList.size();
    }

}
