package br.cantony.clover.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.cantony.clover.R;

/**
 * Created by celio on 29/03/18.
 */

public class NumbersGamesAdapter extends RecyclerView.Adapter<NumbersGamesAdapter.ViewHolder> {

    private ArrayList<Integer> numbers;
    public NumbersGamesAdapter(ArrayList<Integer> numbers) {
        this.numbers = numbers;
    }

    @Override
    public NumbersGamesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_layout_numbers, parent, false);

        NumbersGamesAdapter.ViewHolder viewHolder = new NumbersGamesAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NumbersGamesAdapter.ViewHolder holder, int position) {
        holder.infoText.setText(numbers.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView infoText;
        public ViewHolder(View itemView) {
            super(itemView);

            infoText = (TextView) itemView.findViewById(R.id.info_text);
        }
    }

    public void update(ArrayList<Integer> newDataSet) {
        numbers.clear();
        numbers = newDataSet;
        notifyDataSetChanged();
    }
}
