package br.cantony.clover.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.cantony.clover.R;
import br.cantony.clover.interfaces.OnItemClickListenerMyGames;
import br.cantony.clover.model.entity.MyGames;

/**
 * Created by celio on 25/03/18.
 */

public class MyGamesAdapter extends  RecyclerView.Adapter<MyGamesAdapter.ViewHolder>{

    private final OnItemClickListenerMyGames listener;
    private ArrayList<MyGames> myGamesList;

    public MyGamesAdapter(ArrayList<MyGames> myGamesList,
                          OnItemClickListenerMyGames listener) {
        this.myGamesList = myGamesList;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cvItemsMyGames;
        public TextView tvItemsMyNumbers;
        public TextView tvDtEmit;
        public TextView tvCompetitionNumber;
        public TextView tvCompetition;
        public TextView tvWin;

        public ViewHolder(View itemView) {
            super(itemView);

            cvItemsMyGames = (CardView) itemView.findViewById(R.id.cv_items_mygames);
            tvItemsMyNumbers = (TextView) itemView.findViewById(R.id.label_numbers);
            tvDtEmit = (TextView) itemView.findViewById(R.id.label_dt_emit_number);
            tvCompetitionNumber = (TextView) itemView.findViewById(R.id.label_comp_number);
            tvCompetition = (TextView) itemView.findViewById(R.id.label_comp_resp);
            tvWin = (TextView) itemView.findViewById(R.id.label_win_resp);

        }

        public void bind(final MyGames item,
                         final OnItemClickListenerMyGames listener, final int position) {
//            long click
            cvItemsMyGames.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onItemLongClick(item, position);
                    return false;
                }
            });
//            short click
            cvItemsMyGames.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemShortClick(item);
                }
            });
        }
    }

    @Override
    public MyGamesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_layout_save_my_games, parent, false);

        MyGamesAdapter.ViewHolder viewHolder = new MyGamesAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyGamesAdapter.ViewHolder holder, int position) {
        holder.tvItemsMyNumbers.setText(
                myGamesList.get(position).getDezenasText().replace(",","-"));
        holder.tvDtEmit.setText(myGamesList.get(position).getDataGerada());
        holder.tvCompetitionNumber.setText(myGamesList.get(position).getConcursoApostado()+"");

        if(myGamesList.get(position).getApostado()) {
            holder.tvCompetition.setText("SIM");
        } else {
            holder.tvCompetition.setText("NÃO");
        }

        if(myGamesList.get(position).getGanhou()) {
            holder.tvWin.setText("SIM");
        } else {
            holder.tvWin.setText("NÃO");
        }

        holder.bind(myGamesList.get(position), listener, position);
    }

    @Override
    public int getItemCount() {
        return myGamesList.size();
    }

    public void update(ArrayList<MyGames> myMegaSenasList) {
        if(myMegaSenasList == null || myMegaSenasList.size() == 0)
            return;

        if(myMegaSenasList != null && myMegaSenasList.size() > 0)
            myMegaSenasList.clear();

        this.myGamesList.addAll(myMegaSenasList);
        notifyDataSetChanged();
    }
}
