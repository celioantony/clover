package br.cantony.clover.helper;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import br.cantony.clover.R;
import br.cantony.clover.model.entity.MyGames;

/**
 * Created by celio on 25/03/18.
 */

public class MyGamesHelper {

    private static final String TAG = "GamesHelper";
    private static final int MIN = 1;
    private static final int MAX = 60;
//    private static final int LIMIT = 6;

    private MyGames myGames;
    private TextView tvNumbersGenerated;
    private ImageView btnGenerateNumbers;
    private ImageView btnSaveNumbers;

    public MyGamesHelper(View view) {
//        tvNumbersGenerated = (TextView) view.findViewById(R.id.ms_numbers_generated);
        btnGenerateNumbers = (ImageView) view.findViewById(R.id.ms_btn_generate_numbers);
        btnSaveNumbers = (ImageView) view.findViewById(R.id.ms_btn_save_numbers);
        myGames = new MyGames();


    }

    public ArrayList<Integer> generateNumbers(String type) {
        GamesHelper gamesHelper = new GamesHelper();
        int LIMIT = gamesHelper.getLimit(type);
        ArrayList<Integer> numbers = new ArrayList<>();
        Random r = new Random();
        for(int i = 0; i < LIMIT; i++) {
            int n = r.nextInt(MAX - MIN) + MIN;
            numbers.add(n);
        }
        Collections.sort(numbers);
        return numbers;
    }

    private int[] getList(ArrayList<Integer> numbers) {
        int[] ret = new int[numbers.size()];
        for (int i=0; i < ret.length; i++) {
            ret[i] = numbers.get(i).intValue();
        }
        return ret;
    }

    public MyGames getMyGames(ArrayList<Integer> numbers, String type) {
//        myGames.setDezenas(getMsDezenasList(tvNumbersGenerated.getText().toString()));
        myGames.setDezenas(this.getList(numbers));
        myGames.setConcursoApostado(0000);
        myGames.setApostado(false);
        myGames.setGanhou(false);
        myGames.setComentario("");
        myGames.setTipo(type);

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        String date = (String) df.format("dd/MM/yyyy", new Date());
        myGames.setDataGerada(date);

        return myGames;
    }
}
