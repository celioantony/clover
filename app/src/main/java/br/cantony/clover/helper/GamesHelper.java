package br.cantony.clover.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import br.cantony.clover.model.entity.Games;
import br.cantony.clover.model.entity.MegaSena;

/**
 * Created by celio on 21/03/18.
 */

public class GamesHelper {

    HashMap<String, String> map;
    private HashMap<String, Integer> sizeGame;

    public GamesHelper() {
        this.map = new HashMap<>();
        this.map.put("MS", "MEGA-SENA");
        this.map.put("QN", "QUINA");
        this.map.put("LF", "LOTOFÁCIL");
        this.map.put("LM", "LOTOMANIA");

        sizeGame = new HashMap<>();
        sizeGame.put("MS", 6);
        sizeGame.put("QN", 5);
        sizeGame.put("LF", 15);
        sizeGame.put("LM", 20);
    }

    public ArrayList<Games> getListGames() {
        ArrayList<Games> gameList = new ArrayList<>();

        Games game1 = new Games();
        game1.setInitials("MS");
        game1.setName("MEGA-SENA");

        gameList.add(game1);

        Games game2 = new Games();

        game2.setInitials("QN");
        game2.setName("QUINA");

        gameList.add(game2);

        Games game3 = new Games();

        game3.setInitials("LF");
        game3.setName("LOTOFÁCIL");

        gameList.add(game3);

        Games game4 = new Games();

        game4.setInitials("LM");
        game4.setName("LOTOMANIA");

        gameList.add(game4);

        return gameList;
    }

    public String getDescriptionGame(String key) {
        return this.map.get(key);
    }

    public int getLimit(String key) {
        return this.sizeGame.get(key);
    }

}
