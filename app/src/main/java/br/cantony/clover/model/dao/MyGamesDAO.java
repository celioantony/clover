package br.cantony.clover.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import br.cantony.clover.helper.GamesHelper;
import br.cantony.clover.model.entity.MyGames;

/**
 * Created by celio on 25/03/18.
 */

public class MyGamesDAO extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String TABLE = "MyGames";
    private static final String DB = "Clover";
    private static final String TAG = MyGamesDAO.class.getCanonicalName();

    public MyGamesDAO(Context context) {
        super(context, DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String ddl = "CREATE TABLE "+TABLE+
                        "(id INTEGER PRIMARY KEY, "+
                        "dezenas TEXT, "+
                        "dataGerada TEXT, "+
                        "concursoApostado INTEGER, "+
                        "apostado INTEGER, "+
                        "ganhou INTEGER, "+
                        "comentario TEXT,"+
                        "tipo TEXT"+
                        ")";

        db.execSQL(ddl);
        Log.i(TAG, "Criação da tabela "+TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String ddl = "DROP TABLE IF EXISTS"+TABLE;
        db.execSQL(ddl);
        onCreate(db);
        Log.i(TAG, "Atualização da tabela: "+TABLE);
    }

    public void add(MyGames myGames) {
        ContentValues values = new ContentValues();
        values.put("dezenas", myGames.getDezenasText());
        values.put("dataGerada", myGames.getDataGerada());
        values.put("concursoApostado", myGames.getConcursoApostado());
        values.put("apostado",myGames.getApostado());
        values.put("ganhou", myGames.getGanhou());
        values.put("comentario", myGames.getComentario());
        values.put("tipo", myGames.getTipo());

        getWritableDatabase().insert(TABLE, null, values);

        Log.d(TAG, "Dados de MyMegaSena salvos.");
    }

    public ArrayList<MyGames> list(String type) {
        ArrayList<MyGames> list = new ArrayList<>();
        String sql = "SELECT * FROM "
                +TABLE+" WHERE tipo=\""+type+"\" ORDER BY id DESC";
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        try {
            while (cursor.moveToNext()) {
                MyGames mg = new MyGames();
                mg.setId(cursor.getInt(0));
                mg.setDezenasTextToInt(cursor.getString(1));
                mg.setDataGerada(cursor.getString(2));
                mg.setConcursoApostado(cursor.getInt(3));
                mg.setApostado(cursor.getInt(4)>0);
                mg.setGanhou(cursor.getInt(5)>0);
                mg.setComentario(cursor.getString(6));
                mg.setTipo(cursor.getString(7));

                list.add(mg);
            }

        }catch (SQLException e) {
            Log.e(TAG,e.getMessage());
        }finally {
            cursor.close();
        }
        return list;
    }

    public void update(MyGames myMegaSena) {
        ContentValues values = new ContentValues();

        Log.d(TAG, myMegaSena.toString());

        values.put("concursoApostado", myMegaSena.getConcursoApostado());
        values.put("apostado", myMegaSena.getApostado());
        values.put("ganhou", myMegaSena.getGanhou());
        values.put("comentario", myMegaSena.getComentario());

        String[] args = { String.valueOf(myMegaSena.getId()) };

        getWritableDatabase().update(TABLE, values,
                "id=?", args);
    }

    public void delete(MyGames myGame) {
        String[] args = {String.valueOf(myGame.getId())};
        getWritableDatabase().delete(TABLE,
                "id=?", args);

        GamesHelper gamesHelper = new GamesHelper();
        Log.i(TAG, "Jogo da " +
                ""+gamesHelper.getDescriptionGame(myGame.getTipo())+" foi excluído");
    }

    public int count(){
        String sql = "SELECT COUNT(*) FROM "+TABLE;
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        cursor.moveToFirst();
        int count= cursor.getInt(0);
        cursor.close();

        return count;
    }
}
