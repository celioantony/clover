package br.cantony.clover.model.entity;

/**
 * Created by celio on 20/03/18.
 */

public class MegaSena {
    private long concurso;
    private String dataSorteio;
    private int[] dezenas;
    private float arrecadacaoTotal;
    private int ganhadores;
    private String[] cidade;
    private String[] UF;
    private float rateioSena;
    private int ganhadoresQuina;
    private float rateioQuina;
    private int ganhadoresQuadra;
    private float rateioQuadra;
    private boolean acumulado;
    private float valorAcumulado;
    private float estimativaPremio;
    private float acumuladoMegaVirada;

    public MegaSena() {
    }

    public MegaSena(long concurso, String dataSorteio, int[] dezenas,
                    float arrecadacaoTotal, int ganhadores, String[] cidade,
                    String[] UF, float rateioSena, int ganhadoresQuina,
                    float rateioQuina, int ganhadoresQuadra, float rateioQuadra,
                    boolean acumulado, float valorAcumulado, float estimativaPremio,
                    float acumuladoMegaVirada){

        this.concurso = concurso;
        this.dataSorteio = dataSorteio;
        this.dezenas = dezenas;
        this.arrecadacaoTotal = arrecadacaoTotal;
        this.ganhadores = ganhadores;
        this.cidade = cidade;
        this.UF = UF;
        this.rateioSena = rateioSena;
        this.ganhadoresQuina = ganhadoresQuina;
        this.rateioQuina = rateioQuina;
        this.ganhadoresQuadra = ganhadoresQuadra;
        this.rateioQuadra = rateioQuadra;
        this.acumulado = acumulado;
        this.valorAcumulado = valorAcumulado;
        this.estimativaPremio = estimativaPremio;
        this.acumuladoMegaVirada = acumuladoMegaVirada;

    }

    public long getConcurso() {
        return concurso;
    }

    public void setConcurso(long concurso) {
        this.concurso = concurso;
    }

    public String getDataSorteio() {
        return dataSorteio;
    }

    public void setDataSorteio(String dataSorteio) {
        this.dataSorteio = dataSorteio;
    }

    public int[] getDezenas() {
        return dezenas;
    }

    public void setDezenas(int[] dezenas) {
        this.dezenas = dezenas;
    }

    public float getArrecadacaoTotal() {
        return arrecadacaoTotal;
    }

    public void setArrecadacaoTotal(float arrecadacaoTotal) {
        this.arrecadacaoTotal = arrecadacaoTotal;
    }

    public int getGanhadores() {
        return ganhadores;
    }

    public void setGanhadores(int ganhadores) {
        this.ganhadores = ganhadores;
    }

    public String[] getCidade() {
        return cidade;
    }

    public void setCidade(String[] cidade) {
        this.cidade = cidade;
    }

    public String[] getUF() {
        return UF;
    }

    public void setUF(String[] UF) {
        this.UF = UF;
    }

    public float getRateioSena() {
        return rateioSena;
    }

    public void setRateioSena(float rateioSena) {
        this.rateioSena = rateioSena;
    }

    public int getGanhadoresQuina() {
        return ganhadoresQuina;
    }

    public void setGanhadoresQuina(int ganhadoresQuina) {
        this.ganhadoresQuina = ganhadoresQuina;
    }

    public float getRateioQuina() {
        return rateioQuina;
    }

    public void setRateioQuina(float rateioQuina) {
        this.rateioQuina = rateioQuina;
    }

    public int getGanhadoresQuadra() {
        return ganhadoresQuadra;
    }

    public void setGanhadoresQuadra(int ganhadoresQuadra) {
        this.ganhadoresQuadra = ganhadoresQuadra;
    }

    public float getRateioQuadra() {
        return rateioQuadra;
    }

    public void setRateioQuadra(float rateioQuadra) {
        this.rateioQuadra = rateioQuadra;
    }

    public boolean isAcumulado() {
        return acumulado;
    }

    public void setAcumulado(boolean acumulado) {
        this.acumulado = acumulado;
    }

    public float getValorAcumulado() {
        return valorAcumulado;
    }

    public void setValorAcumulado(float valorAcumulado) {
        this.valorAcumulado = valorAcumulado;
    }

    public float getEstimativaPremio() {
        return estimativaPremio;
    }

    public void setEstimativaPremio(float estimativaPremio) {
        this.estimativaPremio = estimativaPremio;
    }

    public float getAcumuladoMegaVirada() {
        return acumuladoMegaVirada;
    }

    public void setAcumuladoMegaVirada(float acumuladoMegaVirada) {
        this.acumuladoMegaVirada = acumuladoMegaVirada;
    }
}
