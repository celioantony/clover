package br.cantony.clover.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import br.cantony.clover.model.entity.MegaSena;

/**
 * Created by celio on 20/03/18.
 */

public class MegaSenaDAO extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String TABLE = "MegaSena";
    private static final String DB = "Clover";
    private static final String TAG = MegaSenaDAO.class.getCanonicalName();

    public MegaSenaDAO(Context context) {
        super(context, DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String ddl = "CREATE TABLE "+TABLE+
                        "(id INTEGER PRIMARY KEY, "+
                        "concurso INTEGER, "+
                        "dataSorteio TEXT, "+
                        "dezenas TEXT, "+
                        "arrecadacaoTotal REAL, "+
                        "ganhadores INTEGER, "+
                        "cidade TEXT, "+
                        "UF TEXT, "+
                        "rateioSena REAL, "+
                        "ganhadoresQuina INTEGER, "+
                        "rateioQuina REAL, "+
                        "ganhadoresQuadra INTEGER, "+
                        "rateioQuadra REAL, "+
                        "acumulado int, "+
                        "valorAcumulado REAL, "+
                        "estimativaPremio, "+
                        "acumuladoMegaVirada REAL"+
                        ")";

        db.execSQL(ddl);
        Log.i(TAG, "Criação da tabela "+TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String ddl = "DROP TABLE IF EXISTS"+TABLE;
        db.execSQL(ddl);
        onCreate(db);
        Log.i(TAG, "Atualização da tabela: "+TABLE);
    }

    public void add(MegaSena megaSena) {
        ContentValues values = new ContentValues();
        values.put("concurso", megaSena.getConcurso());
        values.put("dataSorteio",megaSena.getDataSorteio());
        values.put("dezenas",megaSena.getDezenas().toString());
        values.put("arrecadacaoTotal",megaSena.getArrecadacaoTotal());
        values.put("ganhadores", megaSena.getGanhadores());
        values.put("cidade", megaSena.getCidade().toString());
        values.put("UF", megaSena.getUF().toString());
        values.put("rateioSena", megaSena.getRateioSena());
        values.put("ganhadoresQuina",megaSena.getGanhadoresQuina());
        values.put("rateioQuina", megaSena.getRateioQuina());
        values.put("ganhadoresQuadra", megaSena.getGanhadoresQuadra());
        values.put("rateioQuadra", megaSena.getRateioQuadra());
        values.put("acumulado", megaSena.isAcumulado());
        values.put("valorAcumulado", megaSena.getValorAcumulado());
        values.put("estimativaPremio", megaSena.getEstimativaPremio());
        values.put("acumuladoMegaVirada", megaSena.getAcumuladoMegaVirada());

        getWritableDatabase().insert(TABLE, null, values);
    }

    public ArrayList<MegaSena> list() {
        ArrayList<MegaSena> list = new ArrayList<>();
        String sql = "SELECT * FROM "+TABLE+" ORDER BY concurso";
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        try {
            while (cursor.moveToNext()) {
                MegaSena ms = new MegaSena();
                ms.setConcurso(cursor.getInt(0));
                ms.setDataSorteio(cursor.getString(1));
                ms.setArrecadacaoTotal(cursor.getFloat(2));
                ms.setGanhadores(cursor.getInt(3));
                ms.setRateioSena(cursor.getFloat(4));
                ms.setGanhadoresQuina(cursor.getInt(5));
                ms.setRateioQuina(cursor.getFloat(6));
                ms.setGanhadoresQuadra(cursor.getInt(7));
                ms.setRateioQuadra(cursor.getFloat(8));
                ms.setAcumulado(cursor.getInt(9)>0);
                ms.setValorAcumulado(cursor.getFloat(10));
                ms.setEstimativaPremio(cursor.getFloat(11));
                ms.setAcumuladoMegaVirada(cursor.getFloat(12));

                list.add(ms);
            }

        }catch (SQLException e) {

        }finally {
            cursor.close();
        }
        return list;
    }
}
