package br.cantony.clover.model.entity;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by celio on 25/03/18.
 */

public class MyGames implements Serializable {

    private long id;
    private int[] dezenas;
    private String dataGerada;
    private int concursoApostado;
    private Boolean apostado;
    private Boolean ganhou;
    private String comentario;
    private String tipo;

    public MyGames(){

    }

    public MyGames(int[] dezenas,
                   String dataGerada,
                   int concursoApostado,
                   Boolean apostado,
                   Boolean ganhou,
                   String comentario) {

        this.dezenas = dezenas;
        this.dataGerada = dataGerada;
        this.concursoApostado = concursoApostado;
        this.apostado = apostado;
        this.ganhou = ganhou;
        this.comentario = comentario;
        this.tipo = tipo;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int[] getDezenas() {
        return dezenas;
    }

    public String getDezenasText() {
        String[] dezenasStr = new String[this.dezenas.length];
        for(int i = 0; i < this.dezenas.length; i++) {
            dezenasStr[i] = String.valueOf(this.dezenas[i]);
        }
        return TextUtils.join(",", dezenasStr);
    }

    public void setDezenas(int[] dezenas) {
        this.dezenas = dezenas;
    }

    public void setDezenasTextToInt(String valueStr) {
        String[] dezenasStr = valueStr.split(",");
        int[] dezenasInt = new int[dezenasStr.length];
        for(int i = 0; i < dezenasStr.length; i++) {
            dezenasInt[i] = Integer.parseInt(dezenasStr[i]);
        }

        this.dezenas = dezenasInt;
    }


    public String getDataGerada() {
        return dataGerada;
    }

    public void setDataGerada(String dataGerada) {
        this.dataGerada = dataGerada;
    }

    public int getConcursoApostado() {
        return concursoApostado;
    }

    public void setConcursoApostado(int concursoApostado) {
        this.concursoApostado = concursoApostado;
    }

    public Boolean getApostado() {
        return apostado;
    }

    public void setApostado(Boolean apostado) {
        this.apostado = apostado;
    }

    public Boolean getGanhou() {
        return ganhou;
    }

    public void setGanhou(Boolean ganhou) {
        this.ganhou = ganhou;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
