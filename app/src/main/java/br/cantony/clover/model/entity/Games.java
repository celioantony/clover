package br.cantony.clover.model.entity;

/**
 * Created by celio on 21/03/18.
 */

public class Games {

    private String name;
    private String initials;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }
}
